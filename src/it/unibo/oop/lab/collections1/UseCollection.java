package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

	private static final int RANGE_START = 1000;
	private static final int RANGE_FINISH = 2000;
	private static final int ELEMS = 10_00000;
	private static final int TO_MS = 1_000_000;
    
	private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	List<Integer> list = new ArrayList<>();
    	
    	for(int i = RANGE_START; i < RANGE_FINISH ; i++) {
    		list.add(i);
    	}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	List<Integer> list2 = new LinkedList<>();
    	list2.addAll(list);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	int first = list.get(0);
    	
    	list.set(0, list.get(list.size()-1));
    	list.set(list.size()-1, first);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for(Integer elem : list ) {
    		System.out.println(elem);
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	
    	for(int i = 1 ; i<= ELEMS ; i++ ) {
    		list2.add(0, i);
    	}
    	
    	 time = System.nanoTime() - time;
         System.out.println("Adding " + ELEMS
                 + " on the head of the list took " + time
                 + "ns (" + time / TO_MS + "ms)");
         System.out.println(list2);
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
         time = System.nanoTime();
         
         for(int i = 1; i <= RANGE_START; i++ ) {
        	 list.get(list.size()/2);
         }
         
         time = System.nanoTime() - time;
         System.out.println("Reading "+RANGE_START 
        		 +" elements on the middle of the Arraylist took "+ time
                 + "ns (" + time / TO_MS + "ms)");
         
         time = System.nanoTime();
         
         for(int i = 1; i <= RANGE_START; i++ ) {
        	 list.get(list.size()/2);
        	 list2.get(list2.size()/2);
         }
         time = System.nanoTime() - time;
         System.out.println("Reading "+RANGE_START 
        		 +" elements on the middle of the Linkedlist took "+ time
                 + "ns (" + time / TO_MS + "ms)");
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
         Map<String,Long> continents = new HashMap<>();
         continents.put("Africa", 1_110_635_000L);
         continents.put("Americas", 972_005_000L);
         continents.put("Antartica", 0L);
         continents.put("Asia", 4_298_723_000L);
         continents.put("Europe", 742_452_000L);
         continents.put("Oceania", 38_304_000L);
         
        /*
         * 8) Compute the population of the world
         */
         long population = continents.get("Africa").longValue() + continents.get("Americas").longValue() +
        		 		   continents.get("Antartica").longValue() + continents.get("Asia").longValue()+
        		 		  continents.get("Europe").longValue() + continents.get("Oceania").longValue();
        		 		
    }
}
